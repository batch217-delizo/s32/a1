const http = require("http");

const port = 4000;

const server = http.createServer((req, res) => {
    if(req.url == "/" && req.method == "GET"){
      res.writeHead(200, {"Content-Type" : "text/plain"})
      res.end("Welcome to booking system");
    } else if (req.url == "/profile" && req.method == "GET") {
      res.writeHead(200, {"Content-Type" : "text/plain"})
      res.end("Welcome to your profile!");
    } else if (req.url == "/courses" && req.method == "GET") {
      res.writeHead(200, {"Content-Type" : "text/plain"})
      res.end("Here`s our courses available");
    } else if (req.url == "/addcourses" && req.method == "POST") {
      res.writeHead(200, {"Content-Type" : "text/plain"})
      res.end("add a course to our resources");
    } else if (req.url == "/updatecourses" && req.method == "PUT") {
      res.writeHead(200, {"Content-Type" : "text/plain"})
      res.end("Update a course to our resources");
    } else if(req.url == "/archivecourses" && req.method == "DELETE") {
      res.writeHead(200, {"Content-Type" : "text/plain"})
      res.end("Archive courses to our resources");
    }  else {
      res.writeHead(404, {"Content-Type" : "text/plain"})
      res.end("ERROR: 404 - PAGE NOT FOUND");
    }
});


server.listen(port, console.log(`Server is now accessible at locahost ${port} `));
